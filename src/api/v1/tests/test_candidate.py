from oauth2_provider.settings import oauth2_settings
from oauth2_provider.models import get_access_token_model, get_application_model
from django.contrib.auth import get_user_model
from django.utils import timezone
from rest_framework.test import APITestCase
from django.shortcuts import resolve_url as r

Application = get_application_model()
AccessToken = get_access_token_model()
UserModel = get_user_model()


class CandidatesTest(APITestCase):

    def setUp(self) -> None:
        oauth2_settings._SCOPES = ["read", "write", "scope1", "scope2", "resource1"]

        self.test_user = UserModel.objects.create_user("test_user", "test@example.com", "123456")

        self.application = Application.objects.create(
            name="Word API Test",
            user=self.test_user,
            client_type=Application.CLIENT_CONFIDENTIAL,
            authorization_grant_type=Application.GRANT_AUTHORIZATION_CODE,
        )

        self.access_token = AccessToken.objects.create(
            user=self.test_user,
            scope="read write",
            expires=timezone.now() + timezone.timedelta(seconds=3600),
            token="secret-access-token-key",
            application=self.application
        )

        self.access_token.save()

        self.auth = "Bearer {0}".format(self.access_token.token)

    def post_save(self, document):
        data = {
            'first_name': 'Fabricio',
            'last_name': 'Pereira',
            'document': document,
            'type_document': 2,
            'job': 'PO'
        }

        response = self.client.post(
            r('candidates-list'),
            data=data,
            HTTP_AUTHORIZATION=self.auth,
            format='json'
        )

        return response.json()

    def test_invalid_cnpj(self):
        """ CNPJ correto 28.718.220/0001-74 """
        response = self.post_save('28718220000175')
        self.assertEqual(response['document'], ['CNPJ inválido!'])

    def test_invalid_cpf(self):
        """ CPF correto 696.149.350-57 """
        response = self.post_save('69614935055')
        self.assertEqual(response['document'], ['CPF inválido!'])

    def test_post_candidate(self):
        candidate = self.post_save('28718220000174')
        self.assertEqual(candidate['id'], 1)

    def test_get_candidates(self):
        self.post_save('28718220000174')
        self.post_save('07358761000169')
        response = self.client.get(r('candidates-list'), HTTP_AUTHORIZATION=self.auth).json()
        self.assertEqual(len(response), 2)
