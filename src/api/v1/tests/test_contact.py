from oauth2_provider.settings import oauth2_settings
from oauth2_provider.models import get_access_token_model, get_application_model
from django.contrib.auth import get_user_model
from django.utils import timezone
from rest_framework.test import APITestCase
from django.shortcuts import resolve_url as r

Application = get_application_model()
AccessToken = get_access_token_model()
UserModel = get_user_model()


class CandidatesTest(APITestCase):

    def setUp(self) -> None:
        oauth2_settings._SCOPES = ["read", "write", "scope1", "scope2", "resource1"]

        self.test_user = UserModel.objects.create_user("test_user", "test@example.com", "123456")

        self.application = Application.objects.create(
            name="Worc API Test",
            user=self.test_user,
            client_type=Application.CLIENT_CONFIDENTIAL,
            authorization_grant_type=Application.GRANT_AUTHORIZATION_CODE,
        )

        self.access_token = AccessToken.objects.create(
            user=self.test_user,
            scope="read write",
            expires=timezone.now() + timezone.timedelta(seconds=3600),
            token="secret-access-token-key",
            application=self.application
        )

        self.access_token.save()
        self.auth = "Bearer {0}".format(self.access_token.token)

    def post_save_candidate(self, document):
        data = {
            'first_name': 'Fabricio',
            'last_name': 'Pereira',
            'document': document,
            'type_document': 1,
            'job': 'SM'
        }

        response = self.client.post(
            r('candidates-list'),
            data=data,
            HTTP_AUTHORIZATION=self.auth,
            format='json'
        )

        return response.json()

    def post_save_contact(self, contact_type, candidate):
        data = {
            'type': contact_type,
            'number': '5516996112618',
            'candidate': candidate,
        }

        response = self.client.post(
            r('contacts-list'),
            data=data,
            HTTP_AUTHORIZATION=self.auth,
            format='json'
        )

        return response.json()

    def test_contact_save(self):
        candidate = self.post_save_candidate('40379361000')
        contact = self.post_save_contact('comercial', candidate['id'])
        self.assertEqual(contact['id'], 1)

    def test_contacts_get(self):
        candidate = self.post_save_candidate('40379361000')
        self.post_save_contact('comercial', candidate['id'])
        self.post_save_contact('residencial', candidate['id'])
        response = self.client.get(
            r('contacts-list'),
            HTTP_AUTHORIZATION=self.auth,
            format='json'
        ).json()
        self.assertEqual(len(response), 2)

    def test_duplicate_type_contacts(self):
        candidate = self.post_save_candidate('40379361000')
        self.post_save_contact('comercial', candidate['id'])
        contact = self.post_save_contact('comercial', candidate['id'])
        expected = ['Os campos type, candidate devem criar um set único.']
        self.assertEqual(contact['non_field_errors'], expected)