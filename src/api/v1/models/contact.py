from django.db import models


class Contact(models.Model):

    type = models.CharField(max_length=30, blank=False, null=False)
    number = models.CharField(max_length=15, blank=False, null=False)
    candidate = models.ForeignKey(
        'Candidate',
        on_delete=models.CASCADE,
        db_column='id_candidate',
        related_name='contacts',
        blank=False,
        null=False)
    created_at = models.DateTimeField(auto_now_add=True, blank=False, null=False)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.type

    class Meta:
        unique_together = ('type', 'candidate')
        db_table = 'contacts'
        app_label = 'v1'
        managed = True
        ordering = ['id']