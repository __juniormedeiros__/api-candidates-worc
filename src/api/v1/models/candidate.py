from django.db import models
from ..validators.document_validator import validate_document


class Candidate(models.Model):
    TYPE_DOCUMENTS = (
        (1, 'CPF'),
        (2, 'CNPJ')
    )

    first_name = models.CharField(max_length=50, blank=False, null=False)
    last_name = models.CharField(max_length=100, blank=False, null=False)
    document = models.CharField(max_length=15, blank=False, null=False, unique=True, validators=[validate_document])
    type_document = models.IntegerField(choices=TYPE_DOCUMENTS, blank=False, null=False)
    job = models.CharField(max_length=15, blank=False, null=False)
    created_at = models.DateTimeField(auto_now_add=True, blank=False, null=False)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.first_name + ' ' + self.last_name

    class Meta:
        db_table = 'candidates'
        app_label = 'v1'
        managed = True
        ordering = ['id']