from rest_flex_fields.views import FlexFieldsMixin
from rest_framework.authentication import SessionAuthentication
from rest_framework import viewsets
from rest_framework.permissions import IsAdminUser
from django_filters import rest_framework as filters
from rest_condition import Or

from ..models.candidate import Candidate
from ..serializers.candidate import CandidateSerializer

from oauth2_provider.contrib.rest_framework import TokenHasReadWriteScope, OAuth2Authentication


class CandidateView(FlexFieldsMixin, viewsets.ModelViewSet):
    queryset = Candidate.objects.all()
    serializer_class = CandidateSerializer
    authentication_classes = [OAuth2Authentication, SessionAuthentication]
    permission_classes = [Or(IsAdminUser, TokenHasReadWriteScope)]
    filter_backends = (filters.DjangoFilterBackend,)
    http_method_names = ['get', 'post', 'patch', 'delete']
