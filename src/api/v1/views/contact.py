from oauth2_provider.contrib.rest_framework import OAuth2Authentication, TokenHasReadWriteScope
from rest_condition import Or
from rest_flex_fields.views import FlexFieldsMixin
from rest_framework import viewsets
from rest_framework.authentication import SessionAuthentication
from rest_framework.permissions import IsAdminUser
from django_filters import rest_framework as filters

from ..models.contact import Contact
from ..serializers.contact import ContactSerializer


class ContactView(FlexFieldsMixin, viewsets.ModelViewSet):
    queryset = Contact.objects.all()
    serializer_class = ContactSerializer
    authentication_classes = [OAuth2Authentication, SessionAuthentication]
    permission_classes = [Or(IsAdminUser, TokenHasReadWriteScope)]
    filter_backends = (filters.DjangoFilterBackend,)
    http_method_names = ['get', 'post', 'patch', 'delete']
