from .views import candidate, contact
from django.conf.urls import url, include
from rest_framework.routers import SimpleRouter

router = SimpleRouter(trailing_slash=False)
router.register('candidates', candidate.CandidateView, basename='candidates')
router.register('contacts', contact.ContactView, basename='contacts')

urlpatterns = [
    url(r'^', include(router.urls))
]
