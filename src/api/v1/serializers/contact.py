from rest_flex_fields import FlexFieldsModelSerializer
from ..models.contact import Contact


class ContactSerializer(FlexFieldsModelSerializer):

    class Meta:
        model = Contact
        fields = '__all__'
        read_only_fields = ('id', 'created_at', 'updated_at')
