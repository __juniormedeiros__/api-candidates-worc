from rest_flex_fields import FlexFieldsModelSerializer
from ..models.candidate import Candidate
from ..serializers.contact import ContactSerializer


class CandidateSerializer(FlexFieldsModelSerializer):
    contacts = ContactSerializer(many=True, read_only=True)

    class Meta:
        model = Candidate
        fields = (
            'id', 'first_name', 'last_name', 'document', 'type_document',
            'job', 'contacts', 'created_at', 'updated_at'
        )
        read_only_fields = ('id', 'created_at', 'updated_at')
