# API Candidatos Worc

Principais dependências:

![](https://img.shields.io/badge/Django-3.2.5-critical)
![](https://img.shields.io/badge/djangorestframework-3.12.4-important)
![](https://img.shields.io/badge/Jinja2-3.0.1-success)
![](https://img.shields.io/badge/requests-2.26.0-critical)
![](https://img.shields.io/badge/drf__flex__fields-0.9.1-informational)
![](https://img.shields.io/badge/gunicorn-20.1.0-critical)

> Este projeto foi criado de acordo com as especificações do **@Alfi, @Samuca e @Fabrício**

A API Candidates da Worc tem como o intuito servir como backend para aplicações de cadastro de candidatos bem como
os seus contatos. 

## Algumas funcionalidades 

- Listagem e cadastro de **candidatos**   `` api-candidates/v1/candidates``
- Listagem e cadastro de **contatos** `` api-candidates/v1/contacts``

## Ambiente de desenvolvimento

Pré-requisitos:

- Python na versão especificada no projeto, baixar e instalar em:
  
> [Windows](https://www.python.org/downloads/windows/)

> [Mac](https://www.python.org/downloads/mac-osx/)

> [Linux](https://www.python.org/downloads/source/)


Após fazer o clone do projeto por ssh ou https: 

- ssh: ``git clone git@bitbucket.org:__juniormedeiros__/api-candidates-worc.git``
- https: ``git clone https://__juniormedeiros__@bitbucket.org/__juniormedeiros__/api-candidates-worc.git``

Acessar os fontes do projeto:

    $ cd api-candidates-worc

Instalação da virtualenv:

[https://virtualenv.pypa.io/en/latest/installation.html](https://virtualenv.pypa.io/en/latest/installation.html)

Criação da virtualenv para isolar a instalação das dependências:

    $ virtualenv .venv

Ativação da virtualenv:

    $ source .venv/bin/activate

Realizar a instalação das dependências do projeto:

    $ pip install -r src/requirements.txt

Configurar as variáveis de ambiente para desenvolvimento:

| Ambientes              | Descrição                                            |
| ---------------------- | --------------------------------------------------   |
| PRODUCTION             | Define o uso do API em ambiente de Produção          |
| SANDBOX                | Define o uso da API em ambiente de Testes            |
| DEVELOPMENT            | Define o uso da API em ambiente de Desenvolvimento   |


**IMPORTANTE**: Para o ambientes de produção é utilizado o arquivo  ***src/deploy/envs*** [production.yml] que
define as configurações de variáveis de ambiente e a instância do container DOCKER que executará.

### Ambiente

| Variável    | Descrição                          |
| ----------- | ---------------------------------- |
| ENVIRONMENT | DEVELOPMENT, SANDBOX ou PRODUCTION |

Antes de subir a aplicação é necessário gerar as migrations para criação do banco de dados conforme o 
tipo de **ambiente** escolhido.

    $ python manage.py makemigrations --settings settings.<ENVIRONMENT>

Aplicando as migrações a base de dados escolhida.

    $ python manage.py migrate --settings settings.<ENVIRONMENT>

Criando usuário para acesso a área do admin.

    $ python manage.py createsuperuser --settings settings.<ENVIRONMENT>

Neste momento, já se pode rodar o comando abaixo para executar o projeto no ambiente desejado:

    $ python manage.py runserver --settings settings.<ENVIRONMENT>

### Registrando Aplicação de Autenticação - OAuth2

Acessamos http://<YOUR_URL>:8000/admin e nos autenticamos no admin do django, agora estamos prontos para registrar nossa API de autenticação,
conforme imagem de exemplo abaixo.

* **OBS:** Não altere os campos **Client id** e **Client secret**, eles são autogerados.

![](doc/RegistroApp.png)

- Pronto! Agora temos nossa API de autenticação funcional, vamos testá-la? Para isso vamos executar o seguinte comando:

``` bash
  $ curl -X POST <YOUR_URL>:8000/api-candidates/v1/token/ 
         -H "content-type: application/x-www-form-urlencoded" 
         -d "grant_type=password&client_id=<your client id>&client_secret=<your client secret>&username=<your username>&password=<your password>"
```

* A resposta deve ser a seguinte:
``` python
   { 
      "expires_in": 36000, 
      "refresh_token": <your refresh token>, 
      "access_token": <your access token>, 
      "token_type": "Bearer", 
      "scope": "read write groups"
   }
```

* O campo **expires_in** define o tempo em segundos da validade do token.

### Estrutura do projeto

Estrutura de diretórios / arquivos:

```
.
├── README.md
├── .gitignore
├── deploy/
│   ├── run_api.sh
│   ├── envs/
│   │   └── production.yml
│   └── images/
│       └── Dockerfile
├── doc/
│   ├── DiagramaBaseDados.png
│   ├── DiagramaSequencia.png
│   ├── Worc API.postman_environment.json
│   └── Worc API.postman_collection.json
└── src/
    ├── __init__.py
    ├── manage.py
    ├── settings/
    │   ├── __init__.py
    │   ├── development.py
    │   ├── production.py
    │   └── sandbox.py
    ├── requirements.txt
    └── api/
        ├── __init__.py
        ├── asgi.py
        ├── urls.py
        ├── wsgi.py
        └── v1/
            ├── migrations/
            ├── models/
            ├── serializers/
            ├── tests/
            ├── validators/
            └── views/      
```

### Lista de EndPoints:

- api-candidates/v1/token
- api-candidates/v1/candidates
- api-candidates/v1/candidates/**<id_candidate>**
- api-candidates/v1/contacts
- api-candidates/v1/contacts/**<id_contact>**


### Macro do processo

* Como premissa os EndPoints abaixo utilizam autenticação OAuth2 para qualquer requisição, sendo necessário
passar o Authorization Token no header da requisição, em caso de dúvidas instale o [Postman](https://www.postman.com/downloads/) e importe as colections da pasta doc/ *.json

a) @Alfi e @Fabricio o EndPoint /api-candidates/v1/candidates - é o responsável pela listagem e inclusão de candidatos:

``` python
[
    {
        "id": 2,
        "first_name": "Fabricio",
        "last_name": "Pereira",
        "document": "28718220000174",
        "type_document": 2,
        "job": "SM",
        "contacts": [],
        "created_at": "2021-08-16T09:31:05.561930-03:00",
        "updated_at": "2021-08-16T09:31:05.561930-03:00"
    }
]
```

b) @Fabricio o EndPoint /api-candidates/v1/candidates/**<id_candidate>** - é o responsável pela listagem dos candidatos cadastrados por **id**:

``` python
{
        "id": 1,
        "first_name": "Fabricio",
        "last_name": "Pereira",
        "document": "28718220000174",
        "type_document": 2,
        "job": "SM",
        "contacts": [
            {
                "id": 1,
                "type": "Comercial",
                "number": "5516996112618",
                "created_at": "2021-08-15T10:35:21.333114-03:00",
                "updated_at": "2021-08-15T10:35:21.333114-03:00",
                "candidate": 1
            },
            {
                "id": 2,
                "type": "Residencial",
                "number": "5516996112619",
                "created_at": "2021-08-15T10:35:44.950809-03:00",
                "updated_at": "2021-08-15T10:47:03.500577-03:00",
                "candidate": 1
            },
            {
                "id": 3,
                "type": "Celular",
                "number": "5516996112619",
                "created_at": "2021-08-16T09:33:20.055757-03:00",
                "updated_at": "2021-08-16T09:33:20.055757-03:00",
                "candidate": 1
            }
        ],
        "created_at": "2021-08-15T10:35:11.043838-03:00",
        "updated_at": "2021-08-15T10:50:26.145921-03:00"
    }
```

c) @Alfi o EndPoint **/api-candidate/v1/contacts** - é o responsável pela listagem, inclusão e alteração de contatos.

``` python
    {
        "type": "Celular",
        "number": "5516996112619",
        "candidate": 1
    }
```

- Na listagem é possível visualizar a data de cadastro e alteração que são geradas automaticamente ao gerar a ação.

d) @Alfi o EndPoint /api-candidates/v1/contacts/**<id_contact>** - Responsável pela listagem de um contato por **id**:

``` python
    {
        "id": 1,
        "type": "Comercial",
        "number": "5516996112618",
        "created_at": "2021-08-15T10:35:21.333114-03:00",
        "updated_at": "2021-08-15T10:35:21.333114-03:00",
        "candidate": 1
    }
```
- Nesse endpoint é possível visualizar também a data de criação do contato e quando ele foi alterado.


Diagrama de sequência:

![](doc/DiagramaSequencia.png)

Diagrama da Base de Dados:

![](doc/DiagramaBaseDados.png)

### Release / Deploy

Após configurar o projeto você deve primeiro gerar a imagem Docker **image-candidates:latest**:

    $ docker build -t image-candidates -f deploy/images/Dockerfile .

Com a imagem gerada pode executar o docker-compose para iniciar o container com nome **api-candidates**:

    $ docker-compose -f deploy/envs/production.yml up

Nesse momento a aplicação deve iniciar na porta 8000 do servidor.


### Request / Collections

Para facilitar a manutenção ou utilização dos EndPoint abaixo segue as collections do Postman.

- [Collection do Postamn](docs/Worc API.postman_collection.json)
- [Environment do Postamn](docs/Worc API.postman_environment.json)

### Documentação

Dentro da pasta Docs é possível visualizar arquivos com as documentações:

- [Diagrama de Sequências](docs/DiagramaSequencia.png)

#### Para o suporte

DEFINIR

#### Para o cliente

DEFINIR